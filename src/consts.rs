pub mod consts{
  use types::*;

  pub const DEX_MAGIC_038: [Jbyte;8] = [ 0x64, 0x65, 0x78, 0x0a, 0x30, 0x33, 0x38, 0x00 ];
  pub const DEX_MAGIC_013: [Jbyte;8] = [ 0x64, 0x65, 0x78, 0x0a, 0x30, 0x33, 0x13, 0x00 ];
  pub const DEX_MAGIC_009: [Jbyte;8] = [ 0x64, 0x65, 0x78, 0x0a, 0x30, 0x33, 0x09, 0x00 ];
  pub const LITTLE_ENDIAN: Juint  = 0x12345678;
  pub const BIG_ENDIAN:    Juint  = 0x87654321;
  pub const NO_INDEX:      u8     = 0x00; // Encoded -0x1 as uleb128p1
  pub mod access{
    pub const ACC_PUBLIC:       u32 = 0x00001;
    pub const ACC_PRIVATE:      u32 = 0x00002;
    pub const ACC_PROTECTED:    u32 = 0x00004;
    pub const ACC_STATIC:       u32 = 0x00008;
    pub const ACC_FINAL:        u32 = 0x00010;
    pub const ACC_SYNCHRONIZED: u32 = 0x00020;
    pub const ACC_VOLATILE:     u32 = 0x00040;
    pub const ACC_BRIDGE:       u32 = 0x00040;
    pub const ACC_TRANSIENT:    u32 = 0x00080;
    pub const ACC_VARARGS:      u32 = 0x00080;
    pub const ACC_NATIVE:       u32 = 0x00100;
    pub const ACC_INTERFACE:    u32 = 0x00200;
    pub const ACC_ABSTRACT:     u32 = 0x00400;
    pub const ACC_STRICT:       u32 = 0x00800;
    pub const ACC_SYNTHETIC:    u32 = 0x01000;
    pub const ACC_ANNOTATION:   u32 = 0x02000;
    pub const ACC_ENUM:         u32 = 0x04000;
    pub const ACC_UNUSED:       u32 = 0x08000;
    pub const ACC_CONSTRUCTOR:  u32 = 0x10000;
    pub const ACC_DECLARED_SYNC:u32 = 0x20000;
  }
  pub mod value {
    pub const VAL_EXTRA_MASK: u8 = 0b11100000;

    pub const VAL_BYTE:       u8 = 0x00;
    pub const VAL_SHORT:      u8 = 0x02;
    pub const VAL_CHAR:       u8 = 0x03;
    pub const VAL_INT:        u8 = 0x04;
    pub const VAL_LONG:       u8 = 0x06;
    pub const VAL_FLOAT:      u8 = 0x10;
    pub const VAL_DOUBLE:     u8 = 0x11;
    pub const VAL_METHOD_TYPE:u8 = 0x15;
    pub const VAL_METHOD_HAND:u8 = 0x16;
    pub const VAL_STRING:     u8 = 0x17;
    pub const VAL_TYPE:       u8 = 0x18;
    pub const VAL_FIELD:      u8 = 0x19;
    pub const VAL_METHOD:     u8 = 0x1a;
    pub const VAL_ENUM:       u8 = 0x1b;
    pub const VAL_ARRAY:      u8 = 0x1c;
    pub const VAL_ANNOTATION: u8 = 0x1d;
    pub const VAL_NULL:       u8 = 0x1e;
    pub const VAL_BOOL_FALSE: u8 = 0x1f;
    pub const VAL_BOOL_TRUE:  u8 = 0x3f;
  }
  pub mod types {
    pub const TYPE_HEADER:                     u16 = 0x0000;
    pub const TYPE_STRING_ID:                  u16 = 0x0001;
    pub const TYPE_TYPE_ID:                    u16 = 0x0002;
    pub const TYPE_PROTO_ID:                   u16 = 0x0003;
    pub const TYPE_FIELD_ID:                   u16 = 0x0004;
    pub const TYPE_METHOD_ID:                  u16 = 0x0005;
    pub const TYPE_CLASS_DEF:                  u16 = 0x0006;
    pub const TYPE_CALL_SITE:                  u16 = 0x0007;
    pub const TYPE_METHOD_HANDLE:              u16 = 0x0008;
    pub const TYPE_MAP_LIST:                   u16 = 0x1000;
    pub const TYPE_TYPE_LIST:                  u16 = 0x1001;
    pub const TYPE_ANNOTATION_SET_REF_LIST:    u16 = 0x1002;
    pub const TYPE_ANNOTATION_SET_ITEM:        u16 = 0x1003;
    pub const TYPE_CLASS_DATA_ITEM:            u16 = 0x2000;
    pub const TYPE_CODE_ITEM:                  u16 = 0x2001;
    pub const TYPE_STRING_DATA_ITEM:           u16 = 0x2002;
    pub const TYPE_DEBUG_INTO_ITEM:            u16 = 0x2003;
    pub const TYPE_ANNOTATION_ITEM:            u16 = 0x2004;
    pub const TYPE_ENCODED_ARRAY_ITEM:         u16 = 0x2005;
    pub const TYPE_ANNOTATIONS_DIRECTORY_ITEM: u16 = 0x2006;
  }
}
