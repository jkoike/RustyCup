mod file {
  use types::*;
  #[repr(C, packed)]
  pub struct DexHeader {
    pub magic:      [Jubyte;8],
    pub checksum:    Juint,
    pub signature:  [Jubyte;20],
    pub header_size: Juint,
    pub endian_tag:  Juint,
    pub link:        HeaderItem,
    pub map_off:     Juint,
    pub string_ids:  HeaderItem,
    pub type_ids:    HeaderItem,
    pub proto_ids:   HeaderItem,
    pub field_ids:   HeaderItem,
    pub method_ids:  HeaderItem,
    pub class_defs:  HeaderItem,
    pub data:        HeaderItem
  }
  pub struct HeaderItem {
    pub size: Juint,
    pub off:  Juint
  }
}
