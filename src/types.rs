pub type Jbyte      =  i8;
pub type Jubyte     =  u8;
pub type Jshort     = i16;
pub type Jushort    = u16;
pub type Jint       = i32;
pub type Juint      = u32;
pub type Jlong      = i64;
pub type Julong     = u64;
pub struct JValue {
  pub size:     u8,
  pub val:      u8,
  pub payload: [u8]
}

pub struct JMapItem{
  pub cont:   Jushort,
  pub unused: Jushort,
  pub size:   Juint,
  pub offset: Juint
}

pub struct JMapList {
  pub size:    Juint,
  pub payload: [JMapItem]
}

pub struct JStringId {
  pub offset: Juint
}

pub struct JStringData {
  pub utf16_size: Juint,
  pub payload: [Jubyte]
}
